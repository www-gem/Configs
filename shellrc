### COLORS FOR THE OUTPUT OF THE LS COMMAND
export LS_COLORS='no=0:fi=0:ex=1:di=1;32:ln=36:or=1;40:mi=1;40:pi=31:so=33:bd=44;37:cd=44;37'
  # list of options in the LS_COLORS file in this repository


### DEFINE THE ACTION FOR THE HOME/END/DELETE/INSERT KEYS
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[3~" delete-char
bindkey "\e[2~" overwrite-mode
  # type cat in a terminal, then pressing the keys will give you the right number to use


### DEFAULT TEXT EDITOR AND INTERNET BROWSER
export EDITOR="nvim"
export BROWSER="librewolf"


### CREATE CUSTOM PROMPT(S)
PS1=$'\n\e[1;36m %@ [%.] %# \e[0m\e[4 q' # for zsh
  # \n - new line
  # %# - specifies whether the user is root (#) or otherwise (%)
  # \e[1;36m - applies bold and cyan color to the following text (list of options in the LS_COLORS file on the www-gem gitlab)
  # %@ - time in 12h format
  # %. - current directory (not the full path)
  # \e[0m - exit color-change
  # \e[4 q - show the cursor as underline _
    # 0 -> blinking block
    # 1 -> blinking block (default)
    # 2 -> steady block
    # 3 -> blinking underline
    # 4 -> steady underline
    # 5 -> blinking bar (xterm)
    # 6 -> steady bar (xterm)
  # Note that zsh also offers a right sided prompt with the RPROMPT variable. It uses the same placeholders as the PS1 prompt. 

  ### OTHER ZSH OPTIONS
  # %D - the date in yy-mm-dd format
  # %T - current time of day, in 24-hour format
  # %t or %@ - current time of day, in 12-hour, am/pm format
  # %* - current time of day in 24-hour format, with seconds
  # %w - the date in day-dd format
  # %W - the date in mm/dd/yy format
  # %D{string} - string is formatted using the strftime function. See man page strftime(3) for more details. Various zsh extensions provide numbers with no leading zero or space if the number is a single digit
  # %f - the day of the month
  # %K - the hour of the day on the 24-hour clock
  # %L - the hour of the day on the 12-hour clock
  # %l - the line (tty) the user is logged in on, without ‘/dev/’ prefix. If the name starts with ‘/dev/tty’, that prefix is stripped
  # %M - the full machine hostname
  # %m - the hostname up to the first ‘.’. An integer may follow the ‘%’ to specify how many components of the hostname are desired. With a negative integer, trailing components of the hostname are shown
  # %n - $USERNAME
  # %y - the line (tty) the user is logged in on, without ‘/dev/’ prefix. This does not treat ‘/dev/tty’ names specially
  # %? - the return status of the last command executed just before the prompt
  # %_ - the status of the parser, i.e. the shell constructs (like ‘if’ and ‘for’) that have been started on the command line. If given an integer number that many strings will be printed; zero or negative or no integer means print as many as there are. This is most useful in prompts PS2 for continuation lines and PS4 for debugging with the XTRACE option; in the latter case it will also work non-interactively
  # %^ - the status of the parser in reverse. This is the same as ‘%_’ other than the order of strings. It is often used in RPS2
  # %d or %/ - current working directory. If an integer follows the ‘%’, it specifies a number of trailing components of the current working directory to show; zero means the whole path. A negative integer specifies leading components, i.e. %-1d specifies the first component
  # %~ - as %d and %/, but if the current working directory starts with $HOME, that part is replaced by a ‘~’. Furthermore, if it has a named directory as its prefix, that part is replaced by a ‘~’ followed by the name of the directory, but only if the result is shorter than the full path; Filename Expansion
  # %e - evaluation depth of the current sourced file, shell function, or eval. This is incremented or decremented every time the value of %N is set or reverted to a previous value, respectively. This is most useful for debugging as part of $PS4
  # %h or %! - current history event number
  # %i - the line number currently being executed in the script, sourced file, or shell function given by %N. This is most useful for debugging as part of $PS4
  # %I - the line number currently being executed in the file %x. This is similar to %i, but the line number is always a line number in the file where the code was defined, even if the code is a shell function
  # %j - the number of jobs
  # %L - the current value of $SHLVL
  # %N - the name of the script, sourced file, or shell function that zsh is currently executing, whichever was started most recently. If there is none, this is equivalent to the parameter $0. An integer may follow the ‘%’ to specify a number of trailing path components to show; zero means the full path. A negative integer specifies leading components
  # %x - the name of the file containing the source code currently being executed. This behaves as %N except that function and eval command names are not shown, instead the file where they were defined
  # %c or % or %C - trailing component of the current working directory. An integer may follow the ‘%’ to get more than one component. Unless ‘%C’ is used, tilde contraction is performed first. These are deprecated as %c and %C are equivalent to %1~ and %1/, respectively, while explicit positive integers have the same effect as for the latter two sequences

  ### BASH OPTIONS
  # \$ - specifies whether the user is root (#) or otherwise ($)
  # \d – date (day/month/date)
  # \h – hostname (short)
  # \H – full hostname (domain name)
  # \j – number of jobs being managed by the shell
  # \l – the basename of the shells terminal device
  # \r – carriage return
  # \s – the name of the shell
  # \t – time (hour:minute:second)
  # \A – time, 24-hour, without seconds
  # \u – current username
  # \v – bASH version
  # \V – extra information about the BASH version
  # \w – current working directory ($HOME is represented by ~)
  # \W – the basename of the working directory ($HOME is represented by ~)
  # \[ – start a sequence of non-displayed characters (useful if you want to add a command or instruction set to the prompt)
  # \] – close or end a sequence of non-displayed characters


### CREATE ALIASES
alias name='command to run'
  # format to create an alias for a command