# My config files in self-service

I have started my Linux journey by using multiple pre-existing dotfiles and tried to customize them to my needs. But over the years, I realized that the most efficient way for me to improve my skills and knowledge of Linux-related topics has been to start from raw config files and play with the available options to build them to my needs/liking.
Hence, rather than sharing my own dotfiles, I will try to add to them a list of the main/most common options so you can start building your own config files directly.

## newsboat
 ### 1. config

My newsboat config file with 1 "trick":

- use of a home-made browser script to automatically open feeds' links with your preferred programs (see 3. below)

    browser "~/.config/newsboat/browser >/dev/null 2>&1"

(*>/dev/null 2>&1* will prevent applications from printing messages and warnings over newsboat interface)  
The script has to be executable to work (or you will have to prepend this command with **sh**)

  ### 2. urls
How I've built my feeds and especially a tip on how to combine youtube channels per feeds based on tags

1) lines starting with --- will serve as titles in my feeds

2 ) lines in the Youtube category will combine youtube's feeds based on tags

    "query:Linux:tags # \"linux\""

3 ) youtube's feeds are appended with a ! and a tag. The first one will hide the feed from newsboat display and the tag will tell newsboat which feed should be combined together based on the query made in 2. above

  ### 3. browser
Hitting a number or the key "o' by default will automatically run your preferred program to open feed's/article's links based on their types.

## shellrc
Basic variables/options to configure your shell
